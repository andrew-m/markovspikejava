package uk.org.flowerchild.markovspike;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MarkovChainThing<T> {
    private int frame_length = 6;
    private MarkovTreeDataStructure<T> markovTreeDataStructure;

    private RandomNumberSource randomNumberSource;

    public MarkovChainThing(List<T> rawInput, int frameLength, RandomNumberSource randomNumberSource) {
        this.frame_length = frameLength;
        markovTreeDataStructure = new MarkovTreeDataStructure<>();
        populateStructure(rawInput, markovTreeDataStructure, frame_length);
        this.randomNumberSource = randomNumberSource;
    }

    private void populateStructure(List<T> input, MarkovTreeDataStructure<T> structure, int frame_length) {
        List<T> currentReadingFrame;
        int pointer = 0;
        int timesToIterate = input.size() - frame_length;
        while (pointer <= timesToIterate) {
            currentReadingFrame = getSubArray(input, pointer, frame_length);
            structure.put(currentReadingFrame);
            pointer++;
        }
    }

    private List<T> getSubArray(List<T> input, int startPoint, int frame_length) {
        int endPoint = startPoint + frame_length;
        if (endPoint > input.size()){
            endPoint = input.size();
        }
        return new ArrayList<>(input.subList(startPoint, endPoint));
    }

    public ArrayList<T> generateChain(int length) {
        ArrayList<T> chain = new ArrayList<>();

        MarkovTreeDataStructure<T> structure = findStructureForExistingChain(chain, markovTreeDataStructure).get();

        for (int i = 0; i < length; i++) {
            T nextCharacter = getRandomCharacterFromStructureWithProbability(structure);
            chain.add(nextCharacter);

            int frame_length = this.frame_length;

            structure = findBestStructurePossibleForChain(chain, markovTreeDataStructure, frame_length);
        }
        return chain;
    }

    private MarkovTreeDataStructure<T> findBestStructurePossibleForChain(ArrayList<T> chain, MarkovTreeDataStructure<T> structure, int frame_length) {
        Optional<MarkovTreeDataStructure<T>> structureForChain = findStructureForChainIfPresent(chain, structure, frame_length);
        if (structureForChain.isPresent() && structureForChain.get().getTotalFrequency() > 0) {
            return structureForChain.get();
        }
        return findBestStructurePossibleForChain(chain, structure, frame_length - 1);
    }

    private Optional<MarkovTreeDataStructure<T>> findStructureForChainIfPresent(ArrayList<T> chain, MarkovTreeDataStructure<T> structure, int frame_length) {
        Optional<MarkovTreeDataStructure<T>> structureForOrder;
        List<T> currentFrame = getTrailingChainUpToMaxLength(chain, frame_length);
        structureForOrder = findStructureForExistingChain(currentFrame, structure);
        return structureForOrder;
    }

    private ArrayList<T> getTrailingChainUpToMaxLength(ArrayList<T> chain, int desiredLength) {
        int chainSize = chain.size();

        if (chainSize < desiredLength) {
            desiredLength = chainSize;
        }
        List<T> characters = chain.subList(chainSize - desiredLength, chainSize);

        return new ArrayList<>(characters);
    }

    private Optional<MarkovTreeDataStructure<T>> findStructureForExistingChain(List<T> existingStringFrame, MarkovTreeDataStructure<T> structure) {
        int desiredDepth = existingStringFrame.size();
        ArrayList<T> mutableList = new ArrayList<>(existingStringFrame);
        //happy path
        if (desiredDepth == 0) {
            return Optional.of(structure);
        }

        //find first T
        T character = mutableList.remove(0);

        //get the correct nested structure
        Optional<MarkovTreeDataStructure<T>> nestedMarkovStructureForKey1 = structure.getNestedMarkovStructureForKey(character);
        if (nestedMarkovStructureForKey1.isPresent()) {
            MarkovTreeDataStructure<T> nestedMarkovTreeDataStructureForKey = nestedMarkovStructureForKey1.get();
            return findStructureForExistingChain(mutableList, nestedMarkovTreeDataStructureForKey);
        }

        return Optional.empty();
    }

    private T getRandomCharacterFromStructureWithProbability(MarkovTreeDataStructure<T> markovTreeDataStructure) {
        T keyAtFrequencyPosition;
        int randomPosition = randomNumberSource.nextInt(markovTreeDataStructure.getTotalFrequency());
        keyAtFrequencyPosition = markovTreeDataStructure.getKeyAtFrequencyPosition(randomPosition);
        return keyAtFrequencyPosition;
    }


}
