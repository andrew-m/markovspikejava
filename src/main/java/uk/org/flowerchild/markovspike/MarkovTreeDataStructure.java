package uk.org.flowerchild.markovspike;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class MarkovTreeDataStructure<T> {

    private Map<T, FrequencyAndSubStructure<T>> map = new HashMap<>();
    private AtomicInteger totalFrequency = new AtomicInteger();

    public MarkovTreeDataStructure() {
    }

    public void put(final List<T> readingFrame) {
        List<T> mutatingReadingFrame = readingFrame;
        if (mutatingReadingFrame.size() == 0) {
            return;
        }

        //read off first item - key map

        T firstItem = mutatingReadingFrame.remove(0);
        if (!map.containsKey(firstItem)) {
            map.put(firstItem, new FrequencyAndSubStructure<>());
        }

        FrequencyAndSubStructure<T> frequencyAndSubStructureForItem = map.get(firstItem);
        frequencyAndSubStructureForItem.incrementFrequency();
        totalFrequency.incrementAndGet();

        //pass substring, first char removed, down to subsequent structures.
        frequencyAndSubStructureForItem.passDown(mutatingReadingFrame);
    }

    public int getTotalFrequency() {
        return totalFrequency.get();
    }

    public Optional<MarkovTreeDataStructure<T>> getNestedMarkovStructureForKey(T key) {
        if (map.containsKey(key)) {
            FrequencyAndSubStructure<T> tFrequencyAndSubStructure = map.get(key);
            return Optional.of(tFrequencyAndSubStructure.getNestedMarkovTreeDataStructure());
        }
        return Optional.empty();
    }

    private T getKeyAtFrequencyPosition2(int frequencyPosition, Map<T, FrequencyAndSubStructure<T>> subStructureMap) {
        Set<T> keys = subStructureMap.keySet();
        for (T candidateKey : keys) {
            int frequency = subStructureMap.get(candidateKey).getFrequency();
            if (frequencyPosition <= frequency) {
                return candidateKey;
            } else {
                frequencyPosition = frequencyPosition - frequency;
            }
        }
        throw new UnsupportedOperationException();
    }

    public T getKeyAtFrequencyPosition(int frequencyPosition) {
        return getKeyAtFrequencyPosition2(frequencyPosition, map);
    }

}
