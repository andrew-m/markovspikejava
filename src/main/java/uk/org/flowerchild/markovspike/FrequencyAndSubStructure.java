package uk.org.flowerchild.markovspike;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class FrequencyAndSubStructure<TY> {
    private AtomicInteger frequency = new AtomicInteger();
    private MarkovTreeDataStructure<TY> nestedMarkovTreeDataStructure = new MarkovTreeDataStructure<>();

    public int getFrequency() {
        return frequency.get();
    }

    public void incrementFrequency() {
        frequency.incrementAndGet();
    }

    public void passDown(List<TY> newReadingFrame) {
        nestedMarkovTreeDataStructure.put(newReadingFrame);
    }

    public MarkovTreeDataStructure<TY> getNestedMarkovTreeDataStructure() {
        return nestedMarkovTreeDataStructure;
    }
}
