package uk.org.flowerchild.markovspike.chaingenerators;

import uk.org.flowerchild.markovspike.DefaultRandomNumberSource;
import uk.org.flowerchild.markovspike.MarkovChainThing;

import java.util.ArrayList;

public class MarkovStringGenerator {

    private final MarkovChainThing<Character> characterMarkovChainThing;

    public MarkovStringGenerator(String rawInput, int frameLenth) {

        String input = rawInput.toLowerCase();
        ArrayList<Character> stringAsListOfCharacters = new ArrayList<Character>();
        for (Character character : input.toCharArray()) {
            stringAsListOfCharacters.add(character);
        }

        characterMarkovChainThing = new MarkovChainThing<Character>(stringAsListOfCharacters, frameLenth, new DefaultRandomNumberSource());
    }

    public String generateString(int length) {
        StringBuilder sb = new StringBuilder();


        ArrayList<Character> chain = characterMarkovChainThing.generateChain(length);

        for (Character character : chain) {
            sb.append(character);
        }
        return sb.toString();
    }
}
