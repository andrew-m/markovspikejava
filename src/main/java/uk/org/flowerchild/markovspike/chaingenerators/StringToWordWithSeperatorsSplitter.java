package uk.org.flowerchild.markovspike.chaingenerators;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringToWordWithSeperatorsSplitter {

    public static ArrayList<String> splitWords(String input) {
        Pattern pattern = Pattern.compile("([^A-Za-z0-9\\-\\']+)" +
                "|" +
                "([A-Za-z0-9\\-\\']+)");
        Matcher matcher = pattern.matcher(input);
        List<String> matches = new ArrayList<String>();
        while (matcher.find()) {
            matches.add(matcher.group());
        }

        return new ArrayList<String>(matches);
    }
}
