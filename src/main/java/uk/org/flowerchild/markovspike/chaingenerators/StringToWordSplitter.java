package uk.org.flowerchild.markovspike.chaingenerators;

import java.util.ArrayList;

import static java.util.Arrays.asList;

public class StringToWordSplitter {

    public static ArrayList<String> splitWords(String input) {
        ArrayList<String> strings = new ArrayList<String>();
        String[] split = input.split("[^A-Za-z0-9\\-\\']+");

        strings.addAll(asList(split));
        return strings;
    }

}
