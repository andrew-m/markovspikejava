package uk.org.flowerchild.markovspike.chaingenerators;

import uk.org.flowerchild.markovspike.DefaultRandomNumberSource;
import uk.org.flowerchild.markovspike.MarkovChainThing;

import java.util.ArrayList;

public class MarkovStringByWordGenerator {

    private final MarkovChainThing<String> characterMarkovChainThing;


    public MarkovStringByWordGenerator(String rawInput, int frameLength) {
        ArrayList<String> strings = StringToWordSplitter.splitWords(rawInput);
        characterMarkovChainThing = new MarkovChainThing<String>(strings, frameLength, new DefaultRandomNumberSource());
    }

    public String generateString(int numberOfWords) {
        StringBuilder sb = new StringBuilder();

        ArrayList<String> chain = characterMarkovChainThing.generateChain(numberOfWords);

        for (String word : chain) {
            sb.append(word);
            sb.append(" ");
        }
        return sb.toString().trim();
    }
}
