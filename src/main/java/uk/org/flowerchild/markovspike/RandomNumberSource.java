package uk.org.flowerchild.markovspike;

public interface RandomNumberSource {
    int nextInt(int bound);
}
