package uk.org.flowerchild.markovspike;

import java.util.Random;

public class DefaultRandomNumberSource implements RandomNumberSource {

    private final Random random;

    public DefaultRandomNumberSource() {
        random = new Random();
    }

    @Override
    public int nextInt(int bound) {
        return random.nextInt(bound);
    }
}
