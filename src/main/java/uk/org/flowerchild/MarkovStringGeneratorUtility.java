package uk.org.flowerchild;

import uk.org.flowerchild.markovspike.FrequencyAndSubStructure;

import java.util.Map;
import java.util.Set;

public class MarkovStringGeneratorUtility {

    public <T> T getKeyAtFrequencyPosition2(int frequencyPosition, Map<T, FrequencyAndSubStructure<T>> subStructureMap) {
        Set<T> keys = subStructureMap.keySet();
        for (T candidateKey : keys) {
            int frequency = subStructureMap.get(candidateKey).getFrequency();
            if (frequencyPosition <= frequency) {
                return candidateKey;
            } else {
                frequencyPosition = frequencyPosition - frequency;
            }
        }
        throw new UnsupportedOperationException();
    }
}
