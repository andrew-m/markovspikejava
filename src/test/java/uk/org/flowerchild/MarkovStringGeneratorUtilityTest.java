package uk.org.flowerchild;

import org.junit.Test;
import uk.org.flowerchild.markovspike.FrequencyAndSubStructure;

import java.util.HashMap;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MarkovStringGeneratorUtilityTest {

    @Test
    public void shouldDoTherightThing() throws Exception {
        MarkovStringGeneratorUtility markovStringGeneratorUtility = new MarkovStringGeneratorUtility();
        HashMap<Integer, FrequencyAndSubStructure<Integer>> map = new HashMap<>();

        map.put(1, getFreqAndSubmap(3));
        map.put(2, getFreqAndSubmap(2));
        assertThat(markovStringGeneratorUtility.getKeyAtFrequencyPosition2(1, map), is(1));
        assertThat(markovStringGeneratorUtility.getKeyAtFrequencyPosition2(2, map), is(1));
        assertThat(markovStringGeneratorUtility.getKeyAtFrequencyPosition2(3, map), is(1));
        assertThat(markovStringGeneratorUtility.getKeyAtFrequencyPosition2(4, map), is(2));
        assertThat(markovStringGeneratorUtility.getKeyAtFrequencyPosition2(4, map), is(2));
    }

    private FrequencyAndSubStructure<Integer> getFreqAndSubmap(int freq) {
        FrequencyAndSubStructure<Integer> integerFrequencyAndSubStructure = new FrequencyAndSubStructure<>();
        for (int i = 0; i < freq; i++) {
            integerFrequencyAndSubStructure.incrementFrequency();
        }
        return integerFrequencyAndSubStructure;

    }
}