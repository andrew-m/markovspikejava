package uk.org.flowerchild.markovspike.chaingenerators;

import org.junit.Test;
import uk.org.flowerchild.markovspike.chaingenerators.StringToWordWithSeperatorsSplitter;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertThat;

public class StringToWordWithSeperatorsSplitterTest {

    @Test
    public void shouldSplitWordsWithSpaces() throws Exception {
        ArrayList<String> output = StringToWordWithSeperatorsSplitter.splitWords("hello world 123");

        assertThat(output.size(), is(5));

        assertThat(output, hasItem("hello"));
        assertThat(output, hasItem(" "));
        assertThat(output, hasItem("world"));
        assertThat(output, hasItem(" "));
        assertThat(output, hasItem("123"));
    }

    @Test
    public void shouldSplitWordsWithSpacesAndPunctuationCombined() throws Exception {
        ArrayList<String> output = StringToWordWithSeperatorsSplitter.splitWords("hello world. Wibble!");

        assertThat(output.size(), is(6));

        assertThat(output, hasItem("hello"));
        assertThat(output, hasItem(" "));
        assertThat(output, hasItem("world"));
        assertThat(output, hasItem(". "));
        assertThat(output, hasItem("Wibble"));
        assertThat(output, hasItem("!"));
    }

    @Test
    public void shouldNotSpitOnApostrophes() throws Exception {
        ArrayList<String> output = StringToWordWithSeperatorsSplitter.splitWords("wibble o'clock");

        assertThat(output.size(), is(3));

        assertThat(output, hasItem("wibble"));
        assertThat(output, hasItem(" "));
        assertThat(output, hasItem("o'clock"));
    }
}