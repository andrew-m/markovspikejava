package uk.org.flowerchild.markovspike.chaingenerators;

import org.junit.Test;
import uk.org.flowerchild.markovspike.chaingenerators.StringToWordSplitter;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertThat;

public class StringToWordSplitterTest {

    @Test
    public void shouldSplitWordsUp() throws Exception {
        ArrayList<String> output = StringToWordSplitter.splitWords("hello");

        assertThat(output.size(), is(1));
        assertThat(output, hasItem("hello"));
    }

    @Test
    public void shouldSplitOnSpaces() throws Exception {
        ArrayList<String> output = StringToWordSplitter.splitWords("hello world");

        assertThat(output.size(), is(2));
        assertThat(output, hasItem("hello"));
        assertThat(output, hasItem("world"));
    }

    @Test
    public void shouldSplitOnAnyWhitespace() throws Exception {
        ArrayList<String> output = StringToWordSplitter.splitWords("hello world\twibble\nwobble");

        assertThat(output.size(), is(4));
        assertThat(output, hasItem("hello"));
        assertThat(output, hasItem("world"));
        assertThat(output, hasItem("wibble"));
        assertThat(output, hasItem("wobble"));
    }

    @Test
    public void shouldSplitOnPunctuationExceptHyphen() throws Exception {
        ArrayList<String> output = StringToWordSplitter.splitWords("hello.world,wibble!wobble?fish-word");

        assertThat(output.size(), is(5));

        assertThat(output, hasItem("hello"));
        assertThat(output, hasItem("world"));
        assertThat(output, hasItem("wibble"));
        assertThat(output, hasItem("wobble"));
        assertThat(output, hasItem("fish-word"));
    }

    @Test
    public void shouldSplitOnPunctuationAndWhitespaceExceptHyphen() throws Exception {
        ArrayList<String> output = StringToWordSplitter.splitWords("hello. world,\twibble!\nwobble? fish");

        assertThat(output.size(), is(5));

        assertThat(output, hasItem("hello"));
        assertThat(output, hasItem("world"));
        assertThat(output, hasItem("wibble"));
        assertThat(output, hasItem("wobble"));
        assertThat(output, hasItem("fish"));
    }

    @Test
    public void shouldNotSplitOnApostropheInWords() throws Exception {
        ArrayList<String> output = StringToWordSplitter.splitWords("o'clock isn't");

        assertThat(output.size(), is(2));

        assertThat(output, hasItem("o'clock"));
        assertThat(output, hasItem("isn't"));
    }
}