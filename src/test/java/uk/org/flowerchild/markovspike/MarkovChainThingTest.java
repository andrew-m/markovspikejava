package uk.org.flowerchild.markovspike;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MarkovChainThingTest {

    @Test
    public void shouldUseRngToPickNumbersFromTree() throws Exception {
        final ArrayList<Integer> numberSequence = getSequenceOfIntegers(1, 2);
        RandomNumberSource randomNumberSource = new FakeRandomNumberSource(1, 2);
        MarkovChainThing<Integer> integerMarkovChainThing = new MarkovChainThing<>(numberSequence, 1, randomNumberSource);

        ArrayList<Integer> integers = integerMarkovChainThing.generateChain(5);
        assertThat(integers.size(), is(5));

        assertThat(integers.get(0), is(1));
        assertThat(integers.get(1), is(2));
        assertThat(integers.get(4), is(1));
    }

    @Test
    public void shouldGenerateOutputInProportionToInput() throws Exception {
        final ArrayList<Integer> numberSequence = getSequenceOfIntegers(1, 1, 1, 2, 2);
        RandomNumberSource randomNumberSource = new IncrementingRandomNumberSource();
        MarkovChainThing<Integer> integerMarkovChainThing = new MarkovChainThing<>(numberSequence, 1, randomNumberSource);

        ArrayList<Integer> integers = integerMarkovChainThing.generateChain(5);
        assertThat(integers.size(), is(5));

        assertThat(integers.get(0), is(1));
        assertThat(integers.get(1), is(1));
        assertThat(integers.get(2), is(1));
        assertThat(integers.get(3), is(2));
        assertThat(integers.get(4), is(2));
    }

    @Test
    public void shouldGenerateOutputInProportionToProbabilityGivenPrecedingCharacters() throws Exception {
        /*
        * 1(3)      1(2)
        *           2(1)
        * 2(2)      2(1)
        *           1(1)
        * */
        final ArrayList<Integer> inputDataSequence = getSequenceOfIntegers(1, 1, 1, 2, 2, 1);
        RandomNumberSource randomNumberSource = new FakeRandomNumberSource(1, 1, 2, 3, 1, 2);
        MarkovChainThing<Integer> integerMarkovChainThing = new MarkovChainThing<>(inputDataSequence, 2, randomNumberSource);

        ArrayList<Integer> integers = integerMarkovChainThing.generateChain(5);
        assertThat(integers.size(), is(5));

        assertThat(integers.get(0), is(1));
        assertThat(integers.get(1), is(1));
        assertThat(integers.get(2), is(1));
        assertThat(integers.get(3), is(2));
        assertThat(integers.get(4), is(1));
    }

    private ArrayList<Integer> getSequenceOfIntegers(int... ints) {
        final ArrayList<Integer> numberSequence = new ArrayList<>();
        for (int anInt : ints) {
            numberSequence.add(anInt);
        }
        return numberSequence;
    }

    private class FakeRandomNumberSource implements RandomNumberSource {

        private int[] numbersToGenerate;
        private int index = 0;

        public FakeRandomNumberSource(int... numbersToGenerate) {
            this.numbersToGenerate = numbersToGenerate;
        }

        @Override
        public int nextInt(int bound) {
            if (index >= numbersToGenerate.length) {
                index = 0;
            }
            int chosenNumber = numbersToGenerate[index];
            index++;
            System.out.println(String.format("Fake RNG. Random Number: %d Bound: %d", chosenNumber, bound));
            return chosenNumber;
        }
    }

    private class IncrementingRandomNumberSource implements RandomNumberSource {
        private int counter = 0;

        @Override
        public int nextInt(int bound) {
            counter++;
            if (counter > bound) {
                counter = 0;
            }
            return counter;
        }

    }
}