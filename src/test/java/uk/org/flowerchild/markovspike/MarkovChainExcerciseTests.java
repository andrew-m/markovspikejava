package uk.org.flowerchild.markovspike;

import org.junit.Test;
import uk.org.flowerchild.markovspike.chaingenerators.MarkovStringByWordGenerator;
import uk.org.flowerchild.markovspike.chaingenerators.MarkovStringByWordWithSeperatorsGenerator;
import uk.org.flowerchild.markovspike.chaingenerators.MarkovStringGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MarkovChainExcerciseTests {

    //What's an exercise test? It's like an acceptance test with no assertions! Easier than invoking your app on the command line.

    @Test
    public void ShouldPopulateTests(){
        File file = new File("CompleteWorksOfShakespear.txt");
        doThingForFile(file);
    }

    @Test
    public void ShouldPopulateTestsJaneAusten(){
        File file = new File("CompleteWorksOfJaneAusten.txt");
        doThingForFile(file);
    }

    @Test
    public void shouldGenerateStringByWords() throws Exception {
        File file = new File("CompleteWorksOfShakespear.txt");
        String stringFromFile = getStringFromFile(file);
        MarkovStringByWordGenerator markovStringByWordGenerator = new MarkovStringByWordGenerator(stringFromFile, 5);

        String s = markovStringByWordGenerator.generateString(40);
        System.out.println(s);
    }

    @Test
    public void shouldGenerateStringByWordsAndPunctuation() throws Exception {
        File file = new File("CompleteWorksOfShakespear.txt");
        String stringFromFile = getStringFromFile(file);
        MarkovStringByWordWithSeperatorsGenerator markovStringByWordGenerator = new MarkovStringByWordWithSeperatorsGenerator(stringFromFile, 5);

        String s = markovStringByWordGenerator.generateString(40);
        System.out.println(s);
    }

    private void doThingForFile(File file) {
        long startTime = System.currentTimeMillis();

        String content = getStringFromFile(file);

        MarkovStringGenerator abcdefgh = new MarkovStringGenerator(content, 6);
        long endTime = System.currentTimeMillis();
        long l = endTime - startTime;

        System.out.println("Duration of load and parse of file: " + Long.toString(l));

        String gibberish = abcdefgh.generateString(140);
        System.out.println(gibberish);
    }

    private String getStringFromFile(File file) {
        String content;
        try {
            content = new Scanner(file).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            System.out.println("missing file: " + file.getAbsolutePath());
            throw new RuntimeException("File not found.", e);
        }
        return content;
    }


}